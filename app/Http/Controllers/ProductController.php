<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Request()->has('q')) {
            $q = Request()->q;
            $products = Product::where('name', 'like', '%'. $q .'%')
                                ->orWhere('price', 'like', '%'. $q .'%')
                                ->orWhere('manufactured_date', 'like', '%'. $q .'%')
                                ->orWhere('location', 'like', '%'. $q .'%')
                                ->get()->toArray();

            return response()->json($products, 200);
        }

        if (Request()->has('sort')) {
            $q = Request()->q;
            $products = Product::orderBy(Request()->sort, Request()->by)->get()->toArray();
            return response()->json($products, 200);
        }

        $products = Product::orderBy('name', 'asc')->get()->toArray();;
        return response()->json($products, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['is_available'] = $data['is_available'] == true ? 1 : 0;
        $product = Product::create($data);

        return response()->json($product, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return response()->json($product, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $data = $request->all();
        $data['is_available'] = $data['is_available'] == true ? 1 : 0;
        $product->fill($data)->save();
        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return $this->index();
    }
}
