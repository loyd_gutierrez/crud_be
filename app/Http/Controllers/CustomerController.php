<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Request()->has('q')) {
            $q = Request()->q;
            $customers = Customer::where('name', 'like', '%'. $q .'%')
                                ->orWhere('address', 'like', '%'. $q .'%')
                                ->orWhere('phone', 'like', '%'. $q .'%')
                                ->orWhere('member_since_date', 'like', '%'. $q .'%')
                                ->get()->toArray();
            return response()->json($customers, 200);
        }

        if (Request()->has('sort')) {
            $q = Request()->q;
            $customers = Customer::orderBy(Request()->sort, Request()->by)->get()->toArray();
            return response()->json($customers, 200);
        }

        $customers = Customer::orderBy('name', 'asc')->get()->toArray();;
        return response()->json($customers, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['is_active'] = $data['is_active'] == true ? 1 : 0;
        $customer = Customer::create($data);

        return response()->json($customer, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        return response()->json($customer, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $data = $request->all();
        $data['is_active'] = $data['is_active'] == true ? 1 : 0;
        $customer->fill($data)->save();

        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::where('id', $id);
        $customer->delete();
        return $this->index();
    }
}
